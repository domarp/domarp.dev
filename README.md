# Pramod Jacob's Personal Website

My personal website, powered by [Gatsby](https://www.gatsbyjs.org/).

## 🚀 Development

```
# Spin up a development server at `localhost:8000`
gatsby develop

# Create a production build
gatsby build

# Serve the production build locally
gatsby serve
```
